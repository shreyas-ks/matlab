clc; clear ; close all;
N=100000;
a=0+1.*rand(N,1)  ;
sample_mean_x = 1./N.*sum(a);
for i=4:100000
    if(i < 4)
        y (i) =0;
    else
        y(i) = a(i) - 2.*a(i-1)+ 0.5.*a(i-2)-a(i-3);
    end
    product_xy(i) = a(i) .* y(i);
end
sample_mean2 = 1./N.*sum(product_xy);
sample_mean_y = 1./N.*sum(y);
co_var = sample_mean2 - sample_mean_x .* sample_mean_y;
display(co_var);
clc;
clear;
N=1000;
for i=1:1000
    a=-3+5.*rand(N,1); 
    sample_mean(i) = 1./N.*sum(a);
    sample_sd(i) = sqrt(1./N.*sum((a-sample_mean(i)).^2));
    b(i)=sample_mean(i);
    c(i)=sample_sd(i);
end
bb = sort(b);
cc = sort(c);
fprintf('mean interval (%d,%d)\n',bb(25),bb(975))
fprintf('standard deviation interval (%d,%d)\n',cc(25),cc(975))


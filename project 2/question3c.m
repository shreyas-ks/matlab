clear;
close all;
M = 10;
p = 1./M;
e = ((M) - 1 )./ 2;
temp = 0;
fivepercent = e*(5/100);
for i=1:10
    a =  randi([1,M],1000,1);
    for j=1:10
        temp = temp + a(j).*p ;
    end
    e_o(i)  = temp;
    temp = 0 ;
end
sum = 0;
m=1;
for j=1:10
    temp1 = ((e_o(j)-e)^2)/e;
    observed(j) = temp1;
    central_line(j) = fivepercent;
    if(temp1 < fivepercent)
        fit(m) = temp1;
        m = m+1;
        sum = sum + temp1;
    end
end
plot(observed,'g');hold on;plot(central_line,'b'); hold on; plot(fit,'r'); hold off
fprintf ('The statistical fit is %d\n',sum);

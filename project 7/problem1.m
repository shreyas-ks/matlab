close all; 
clear a;
mu = [1 2 3];
Sigma = [3 -1 1 ; -1 5 3 ; 1 3 4];
A = chol(Sigma);
A_transpose = transpose(A);
A_matrix = A * A_transpose;
for i=1:1000
    z1 = normrnd(0,1);
    z2 = normrnd(0,1);
    z3 = normrnd(0,1);
    x1(i)= A(1,1)*z1 + A(1,2)*z2 + A(1,3)*z3 + mu(1);
    x2(i)= A(2,1)*z1 + A(2,2)*z2 + A(2,3)*z3 + mu(2);
    x3(i)= A(3,1)*z1 + A(3,2)*z2 + A(3,3)*z3 + mu(3);
end
disp('random variable generated is:');
X=[x1(1) x2(1) x3(1)];
disp(X);
temp=subplot(3,1,1),histogram(x1);
subplot(3,1,2),histogram(x2);
subplot(3,1,3),histogram(x3);
title(temp,'Distribution of x1,x2 and x3');

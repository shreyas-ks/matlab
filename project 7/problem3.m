close all; 
clear all;
mean = [0 5;2 0];
sigma_matrix = cat(3,[2 0;0 1],[1 0;0 1]);
weights = [0.75,0.25];
sample_obj = gmdistribution(mean,sigma_matrix,weights);
sample_gen_matrix = random(sample_obj,300);
subplot(2,2,1);
hold on;
opt = statset('Display','final');
max_expectation = gmdistribution.fit(sample_gen_matrix,2,'Options',opt);
ezcontour(@(x,y)pdf(max_expectation,[x y]),[-4 8],[-4 8]);
title('Scatter plot after convergence'); 
xlabel('Range-X'); 
ylabel('Range-Y');
hold off
subplot(2,2,2);
pdf(max_expectation,sample_gen_matrix);
ezsurf(@(x,y)pdf(max_expectation,[x y]),[-5 8],[-5 8])
clear all;
close all;
for i=1:1000
    condition_var = rand();
    if(condition_var <= 0.4)
        value(i) = normrnd(-1,1);
    else
        value(i) = normrnd(1,1);
    end
end

yyaxis left;
histogram(value);
title('PDF of normPDF');
xlabel('Normal Samples');
ylabel('Frequency'); 
yyaxis right;
ylabel('Density');
theo_normal=normpdf(-10:0.1:10,-1,1);
plot(-10:0.1:10,theo_normal,'-');




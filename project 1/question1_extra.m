n=50;
head=0;
no_of_heads=0;
head_run=0;
longest_head_run=0;
experiment=1000;
for k=1:experiment
    for i=1:n
        if(rand > 0.5)
            head=1;
        else
            head=0;
        end
        if(head == 1)
            head_run = head_run + 1;
            no_of_heads = no_of_heads + 1;
        end
        if(head == 0)
            if(longest_head_run < head_run)
                longest_head_run = head_run;
            end
            head_run = 0 ;
        end
    end
    hist_number(k) = no_of_heads;
    no_of_heads = 0;
end
disp('number of tosses')
disp(n)
disp('number of experiments')
disp(experiment)
disp('longest run of heads')
disp(longest_head_run)
histogram(hist_number)
title('fair coin tosses')
xlabel('Number of favourable outcomes')
ylabel('Number of experiments')
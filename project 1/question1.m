n=50;
head=0;
no_of_heads=0;
head_run=0;
longest_head_run=0;
    for i=1:n
        if(rand > 0.5)
            head=1;
            outcomes(i) = 1;
        else
            head=0;
            outcomes(i) = 0;
        end
        if(head == 1)
            head_run = head_run + 1;
            no_of_heads = no_of_heads + 1;
        end
        if(head == 0)
            if(longest_head_run < head_run)
                longest_head_run = head_run;
            end
            head_run = 0 ;
        end
    end
disp('number of tosses')
disp(n)
disp('number of heads')
disp(no_of_heads)
disp('longest run of heads')
disp(longest_head_run)
histogram(outcomes)
title('fair coin tosses')
xlabel('Bernoulli outcomes')
ylabel('Tosees')


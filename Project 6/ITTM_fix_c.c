# include "mex.h"
# include "math.h"

// %% ITTM filter with fixed number of iteration
// % input: data              //input signal
// %        num_iteration     // number of iterations
// %        opt_type          // type of the filter ouput: ITTM1, ITTM2, ITTM3
// 
// 
// %output: output  // filter output
// 
// % created by Miao Zhenwei.
// % Nanyang Technological Univerisity, ROSE center
// % Webpage: https://sites.google.com/site/miaozhenwei/
// 
// 
// % Please report bugs and/or send comments to Miao Zhenwei.
// % zwmiao@ntu.edu.sg
// 
// %  Reference: Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.
// 
// % Related papers
// 
// %             Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.
// %
// %             Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization
// %             of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II, 
// %             vol. 59, no. 11, pp. 810-814, November 2012.
// %  
// %             X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions 
// %             on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.
// %%

double ITTM_Fix(double *x, int n, int itertime, int opt_type)
{
	double		   bh=0,bl=0,mu,tau;
	int            ntauh=0,ntaul=0;	
	int            rn=n,k,i;
	int            ite = itertime;
	int            deltn = 0; 
	double         adp   = 0; 


	//step1 compute the mean
	while (ite>0)
	{    
		ite--;		
		// step 1: compute the mean
		mu = 0;
		for (i=0;i<rn;i++)
			mu += x[i];		
		mu       = (mu+deltn*adp)/(rn+deltn);		
		//step2 compute the dynamic threshold tao
		tau = 0; 
		for (i=0;i<rn;i++)
		{
			tau += fabs(x[i]-mu);
		}
		tau      = (tau+deltn*fabs(adp-mu))/(rn+deltn);
		//step3: remove the untruncatd samples truncate the data
		bl   = mu-tau;
		bh   = mu+tau;
		k     = 0;	
		for (i=0;i<rn;i++)
		{
			if (x[i]>bh)
			{
				ntauh++;
			}
			else
			{
				if (x[i]>=bl)
				{
					x[k] = x[i];
					k++;
				}
				else
					ntaul++;
			}
		}
		rn              =  k;	
		
		if (ntauh>ntaul)
		{
			deltn = ntauh-ntaul;
			adp   = bh; // assign the value of the higher boundary to adp
		}
		else
		{
			deltn = ntaul-ntauh;
			adp   = bl; // assign the value of the lower boundary to adp
		}
	}

    // compute the output
    // output 1
    mu = 0;
    for (i = 0;i<rn;i++)
    {
        mu += x[i];
    }
    if (opt_type==1) // type 1 output
    {
        mu = (mu+bh*ntauh+bl*ntaul)/n;
        return (mu);
    }
    else if (opt_type==2) // type 2 output
    {
        if (rn>n/4)
        {
            mu = mu/rn;
            return (mu);
        }
        else
        {
            mu = (mu+bh*ntauh+bl*ntaul)/n;
            return (mu);
        }
    }
    else if (opt_type == 3) // type 3 output
    {
        if(ntauh>ntaul)
        {
            if ( 2*ntaul<n*0.75)
                mu = (mu+(ntauh-ntaul)*bh)/(n-2*ntaul);
            else
                mu = (mu+bh*ntauh+bl*ntaul)/n;
            return (mu);
        }
        else
        {
            if ( 2*ntauh<n*0.75)
                mu = (mu+(ntaul-ntauh)*bl)/(n-2*ntauh);
            else
                mu = (mu+bh*ntauh+bl*ntaul)/n;
            return (mu);
        }
        
    }                
}


void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    int    n,m,i,opt_type;
    double *data,*opt,*datat;
    int num_iteration;
    
    
    datat          = mxGetPr(prhs[0]);                  // input data
    n              = mxGetN(prhs[0])*mxGetM(prhs[0]);	// get the size of the data
    num_iteration  = (int)mxGetScalar(prhs[1]);         // number of iterations
    opt_type       = (int)mxGetScalar(prhs[2]);         // the type of the output: type 1 or type 2
    
    plhs[0]   = mxCreateDoubleMatrix(1,1,mxREAL);       // output
    opt       = mxGetPr(plhs[0]);
    
    data      =(double *)mxCalloc((int)(n),sizeof(double));
    for (i=0;i<n;i++)
        data[i] = datat[i];
    opt[0] =  ITTM_Fix(data, n, num_iteration, opt_type);
    mxFree(data);
}
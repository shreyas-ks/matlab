clear all;
userpath('/Users/shreyas/Documents/MATLAB/');
alpha = [0.5, 1, 1.8, 2];
beta = 0.75;
for i=1:4
	X = stblrnd(alpha(i), beta, 1, 0, 1000, 1);
	ts = timeseries(X);
	figure(i);
	str = sprintf('Distribution for alpha %f', alpha(i));
	title(str);
	xlabel('Samples');
	yyaxis left;
	y = -50:50;
	hist(X, y);
	xlim([-30, 30]);
	h = findobj(gca, 'Type', 'patch');
	p = stblpdf(y, alpha(i), beta, 1, 0);
	yyaxis right;
	plot(y, p, 'b');
end

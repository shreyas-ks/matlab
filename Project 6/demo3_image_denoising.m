%demo code 3: image denoising

% please compile the c codes before using them

%% 
% created by Miao Zhenwei.
% Nanyang Technological Univerisity, ROSE center
% Webpage: https://sites.google.com/site/miaozhenwei/


% Please report bugs and/or send comments to Miao Zhenwei.
% zwmiao@ntu.edu.sg

% Reference: 

%             Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.

% Related papers

%             Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.
%
%             Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization
%             of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II, 
%             vol. 59, no. 11, pp. 810-814, November 2012.
%  
%             X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions 
%             on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.
clc;clear;close all
%% compile the c code: 
ITTM_compile

%%
filename        = 'lena';
img_org            = double(imread('lena.tif'));
[M N]           = size(img_org);

% add noise %% mixed gaussian and alpha stable noise
alpha           = 1.8;
gama            = 10;
alpha_noise     = stblrnd(alpha,0,gama,0,M,N);% alpha-stable noise 

gau_noise       = randn(M, N)*10;%Gaussian noise
mix_matrix      = rand(M, N);
img_noise       = (mix_matrix>0.5).*alpha_noise+(mix_matrix<=0.5).*gau_noise+img_org; %%mixed noise+image

img_noise(img_noise>255)=255; img_noise(img_noise<0)=0;


nimg_noise_pad    = padarray(img_noise,[2,2],'symmetric');     

%% mean filter
fw  = 5; fn  = fw*fw; fnh = floor(fw/2); hfn=(fn-1)/2+1;
tic
res = LF_c(nimg_noise_pad,5);
toc
img_mean=res(3:end-2,3:end-2);

%% median filter with quick sort
tic
res = median_c(nimg_noise_pad,5);
toc
img_median    = res(3:end-2,3:end-2);


%% the proposed ITTM algorithm, 
tic
res   = ITTM_c2d(nimg_noise_pad,5);
toc
img_ITTM3 = res(3:end-2,3:end-2);

figure
imshow(uint8(img_noise))
figure
imshow(uint8(img_mean))
figure
imshow(uint8(img_median))
figure
imshow(uint8(img_ITTM3))

var_mean = sum(sum(img_mean(fnh+1:M-fnh,fnh+1:N-fnh)-img_org(fnh+1:M-fnh,fnh+1:N-fnh)).^2)/((M-2*fnh)*(N-2*fnh))

var_median = sum(sum(img_median(fnh+1:M-fnh,fnh+1:N-fnh)-img_org(fnh+1:M-fnh,fnh+1:N-fnh)).^2)/((M-2*fnh)*(N-2*fnh))

var_ITTM3 = sum(sum(img_ITTM3(fnh+1:M-fnh,fnh+1:N-fnh)-img_org(fnh+1:M-fnh,fnh+1:N-fnh)).^2)/((M-2*fnh)*(N-2*fnh))


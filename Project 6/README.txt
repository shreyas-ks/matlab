The reference papers are 

Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.

Relevant papers

Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.

Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II, vol. 59, no. 11, pp. 810-814, November 2012.


X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.

More inforation can be found from the webpage:
http://www.ntu.edu.sg/home/exdjiang/default.htm

and my homepage:
https://sites.google.com/site/miaozhenwei/

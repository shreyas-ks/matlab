#include "mex.h"
#include <math.h>

// %% 
// % created by Miao Zhenwei.
// % Nanyang Technological Univerisity, ROSE center
// % Webpage: https://sites.google.com/site/miaozhenwei/
// 
// 
// % Please report bugs and/or send comments to Miao Zhenwei.
// % zwmiao@ntu.edu.sg
// 
// %  Reference: %             Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.
// 
// 
// % Related papers
// 
// %             Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.
// 
// %             Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization
// %             of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II, 
// %             vol. 59, no. 11, pp. 810-814, November 2012.
// %  
// %             X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions 
// %             on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.


int compare (const void * a, const void * b)
{
	if(*(const double*)a < *(const double*)b)         
		return -1;     
	return *(const double*)a > *(const double*)b; 
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *data,*w,*opt,*d_win,sw;
    int m,n,win,f_s,hf;      
    int ii,jj,i,j,count,hw;
    
    double dsum;
    
    data   = mxGetPr(prhs[0]);      // input data
    n      = mxGetN(prhs[0]);		// get the width of the data
    m      = mxGetM(prhs[0]);		// get the length of the data
    win    = (int)mxGetScalar(prhs[1]);
    
    plhs[0]   = mxCreateDoubleMatrix(m,n,mxREAL);    
    opt       = mxGetPr(plhs[0]);   
    
    f_s     = win*win;
    hf      = (int)(win/2);
    hw      = (f_s+1)/2-1;
    d_win   = (double *)mxCalloc((int)(f_s),sizeof(double)); 
    
  
    for (ii=hf;ii<m-hf;ii++)
    {
        for (jj=hf;jj<n-hf;jj++)
        {
            count  = 0;
            for (j=-hf;j<=hf;j++)
                for(i=-hf;i<=hf;i++)
                {
                    d_win[count] = data[(ii+i)+(jj+j)*m];
                    count++;
                }            
            qsort(d_win,f_s,sizeof(double),compare);
            opt[ii+jj*m] = d_win[hw];
            
        }
    }
}



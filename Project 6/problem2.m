clear all;close all;
alpha = 5.5;
theta = 1;
gamma = @(x) 1/gamma(alpha) .* x.^(alpha-1) .* exp(-(x * theta)); 
exp = @(y) (1/alpha) * exp(-y/alpha); 
t = 0:0.1:40;
ratio = gamma(t)./exp(t);
c = max(ratio);
count = 0;
const = (alpha/(c*gamma(alpha)));
for i=1:1000
    while (1)
	r1 = rand;
    val = (-1)*alpha*log(r1);
	r2 = rand;
	if ( r2 < (const * (val^(alpha-1)) * exp((-1) * val * (1-(1/alpha)))) )
        X(i) = val;
        break;
    else
	count = count + 1;
    end
	end
end
yyaxis left;
hist(X,15);
title('PDF of Gamma(5.5, 1) on histogram of gamma');
xlabel('Gamma samples');
ylabel('Frequency'); 
yyaxis right;
ylabel('Density');
theo_gamma=gampdf(0:0.1:40,alpha,1);
plot(0:0.1:40,theo_gamma,'--*');
ylabel('Theoretical gammma'); 
accept_rate=(1000/(count+1000))*100;
disp('Acceptance rate:');
disp(accept_rate);

#include "mex.h"
#include <math.h>

// %% 
// % created by Miao Zhenwei.
// % Nanyang Technological Univerisity, ROSE center
// % Webpage: https://sites.google.com/site/miaozhenwei/
// 
// 
// % Please report bugs and/or send comments to Miao Zhenwei.
// % zwmiao@ntu.edu.sg
// 
// %  Reference: %             Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.
// 
// 
// % Related papers
// 
// %             Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.
// 
// %             Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization
// %             of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II, 
// %             vol. 59, no. 11, pp. 810-814, November 2012.
// %  
// %             X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions 
// %             on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.

double ITTM(double *x, int n, float vp1,float vp2,float vp3,float vp4,int opt_type)
{
	double		   bl=0,bh=0,mu=0,tau;
	int            rn=n,k,i;
	int            ntauh=0,ntaul=0;	
	int            nl,nh,ite=0;
	int            deltnt = 0, deltntm=0;
	int            S1=0,S2=0,S3=0,S4=0;

	

	do
	{
        deltntm  = deltnt;
        //step1 compute the mean
		mu = 0;
		for (i=0;i<rn;i++)
			mu += x[i];
		
		if (ntauh>ntaul)
			mu       = (mu+(ntauh-ntaul)*bh)/(n-2*ntaul);
		else
			mu       = (mu+(ntaul-ntauh)*bl)/(n-2*ntauh);
		
		//step2 compute the dynamic threshold tao
		tau = 0; 
		for (i=0;i<rn;i++)
		{
			tau += fabs(x[i]-mu);
		}

		if (ntauh>ntaul)
			tau      = (tau+(ntauh-ntaul)*(bh-mu))/(n-2*ntaul);
		else
			tau      = (tau+(ntaul-ntauh)*(mu-bl))/(n-2*ntauh);
		
			
		//step3: remove the untruncatd samples truncate the data
		nh	  = 0;
    	nl	  = 0;
		bh    = mu+tau;
		bl    = mu-tau;

		k     = 0;	
        for (i=0;i<rn;i++)
        {
            if (x[i]>bh)
            {
                ntauh++;
            }
            else
            {
                if (x[i]>=bl)
                {
                    if (x[i]>mu)
                        nh++;
                    else
                        nl++;
                    x[k] = x[i];
                    k++;
                }
                else
                    ntaul++;
            }
        }
		
		rn              =  k;
		nh              = nh+ntauh;
		nl              = nl+ntaul;

        // Step 3 check the stop criterion
        //criterion 1
        S1 =abs(nh-nl);
        //criterion 2
        ite++;
        //criterion 3
        deltnt = abs(ntauh-ntaul);
        S3     = abs(ntauh-ntaul);
        // criterion 4
    }
    while(((S1>vp1)&&(ite<vp2)&&(S3<vp3)&&((S3<vp4)||(deltnt!=deltntm))));

	// compute the output
    // output 1
    mu = 0;
    for (i = 0;i<rn;i++)
    {
        mu += x[i];
    }
    if (opt_type==1) // type 1 output
    {
        mu = (mu+bh*ntauh+bl*ntaul)/n;
        return (mu);
    }
    else if (opt_type==2) // type 2 output
    {
        if (rn>n/4)
        {
            mu = mu/rn;
            return (mu);
        }
        else
        {
            mu = (mu+bh*ntauh+bl*ntaul)/n;
            return (mu);
        }
    }
    else if (opt_type == 3) // type 3 output
    {
        if(ntauh>ntaul)
        {
            if ( 2*ntaul<n*0.75)
                mu = (mu+(ntauh-ntaul)*bh)/(n-2*ntaul);
            else
                mu = (mu+bh*ntauh+bl*ntaul)/n;
            return (mu);
        }
        else
        {
            if ( 2*ntauh<n*0.75)
                mu = (mu+(ntaul-ntauh)*bl)/(n-2*ntauh);
            else
                mu = (mu+bh*ntauh+bl*ntaul)/n;
            return (mu);
        }
        
    }       
}


double FITM(double *temp, int l_temp, double vp1,double vp2,double vp3,double vp4)
{
	double		   low=0,high=0,mu=0,tau=0;
	int            rn=l_temp,k=0,i=0;
	int            ntauh=0,ntaul=0;	
	int            nl=0,nh=0,ite=0;
	int            deltnt = 0, deltntm=0;
	int            S1=0,S2=0,S3=0,S4=0;

	

	do
	{	
		deltntm  = deltnt;
		//step1 compute the mean
		mu = 0;
		for (i=0;i<rn;i++)
			mu += temp[i];
		mu       = (mu+ntauh*high+ntaul*low)/l_temp;
		
		//step2 compute the dynamic threshold tao
		tau = 0; 
		for (i=0;i<rn;i++)
		{
            if (temp[i]>mu)
                tau+=temp[i]-mu;
            else
                tau+=mu-temp[i];
// 			tau += (double)abs(double(temp[i]-mu));
		}
		tau   = (tau+ntauh*(high-mu)+ntaul*(mu-low))/l_temp;

		//step3: remove the untruncatd samples truncate the data
		nh	  = 0;
    	nl	  = 0;
		high  = mu+tau;
		low   = mu-tau;

		k     = 0;	
		for (i=0;i<rn;i++)
		{
			if (temp[i]>high)
			{
				ntauh++;
			}
			else
			{
				if (temp[i]>=low)
				{
					if (temp[i]>mu)
						nh++;
					else
						nl++;
					temp[k] = temp[i];
					k++;
				}
				else
					ntaul++;
			}
		}
		
		rn              =  k;
		nh              = nh+ntauh;
		nl              = nl+ntaul;
		//step 3 check the stop criterion
		// criteria 1
		S1 = abs(l_temp-2*nh);
		//S1<=vp1
		// criteria 2
		ite++;
		// ite>=vp2
		// criteria 3

		deltnt = abs(ntauh-ntaul);
		// S3 = deltnt>=vp3
		// criteria 4
		// deltnt == deltntm && deltnt>=vp4
	}
	while((S1>vp1)&&(ite<vp2)&&(deltnt<vp3)&&((deltnt<vp4)||(deltntm!=deltnt)));

	mu = 0;
	for (i=0;i<rn;i++)
		mu += temp[i];
	mu = (mu+ntauh*high+ntaul*low)/l_temp;
	return(mu);	
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *data,*w,*opt,*d_win,sw;
    int m,n,win,f_s,hf;      
    int ii,jj,i,j,count,hw;
     double vp1, vp2, vp3, vp4;

    
    
    double dsum;
    
    data   = mxGetPr(prhs[0]);      // input data
    n      = mxGetN(prhs[0]);		// get the width of the data
    m      = mxGetM(prhs[0]);		// get the length of the data
    win    = (int)mxGetScalar(prhs[1]);
    
    plhs[0]   = mxCreateDoubleMatrix(m,n,mxREAL);    //creat one space for the output 
    opt       = mxGetPr(plhs[0]);   
    
    
    
    f_s     = win*win;
    hf      = (int)(win/2);
    hw      = (f_s+1)/2-1;
//     mexPrintf("m %d  n %d\n",m, n);
    d_win   = (double *)mxCalloc((int)(f_s),sizeof(double)); 
    vp1             = 1;
    vp2             = 2*(double)sqrt((double)f_s);
    vp3             = (n-(double)sqrt((double)f_s))/2;
//  mexPrintf(" vp3   %f\n",vp3);
    vp4             = (double)sqrt((double)f_s);
  
    
    for (ii=hf;ii<m-hf;ii++)
    {
        for (jj=hf;jj<n-hf;jj++)
        {
            count  = 0;
            for (j=-hf;j<=hf;j++)
                for(i=-hf;i<=hf;i++)
                {
                    d_win[count] = data[(ii+i)+(jj+j)*m];
                    count++;
                }                       
//             opt[ii+jj*m] = FITM(d_win, f_s, vp1,vp2,vp3,vp4);
            opt[ii+jj*m] =  ITTM(d_win, f_s, vp1,vp2, vp3, vp4,3);
            
        }
    }
}



%demo1 code

%% 
% created by Miao Zhenwei.
% Nanyang Technological Univerisity, ROSE center
% Webpage: https://sites.google.com/site/miaozhenwei/

% Please report bugs and/or send comments to Miao Zhenwei.
% zwmiao@ntu.edu.sg

%  Reference: [1] Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.

% Related papers

%             Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.
%
%             Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization
%             of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II, 
%             vol. 59, no. 11, pp. 810-814, November 2012.
%  
%             X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions 
%             on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.
%%
clc;clear;close all
%% compile the c code: 
ITTM_compile


%%  
n    = 49; %% filter window size

% input signal: Gaussian noise
xw = randn(n,1);

%% terminated with the proposed stopping criterion in [1]
% c code
yittm1 = ITTM_c(xw,1) % ITTM filter output: ITTM 1
yittm2 = ITTM_c(xw,2) % ITTM filter output: ITTM 2
yittm3 = ITTM_c(xw,3) % ITTM filter output: ITTM 3

% matlab code
yittm1_m = ITTM(xw,1) % ITTM filter output: ITTM 1
yittm2_m = ITTM(xw,2) % ITTM filter output: ITTM 2
yittm3_m = ITTM(xw,3) % ITTM filter output: ITTM 3

%% terminated with a fixed numnber of iterstions
k = 2;%number of iterations
% c code
yittm_fix_1 = ITTM_fix_c(xw,2,1) % ITTM1 filter with a fixed number of iteration 
yittm_fix_2 = ITTM_fix_c(xw,2,2) % ITTM2 filter with a fixed number of iteration
yittm_fix_3 = ITTM_fix_c(xw,2,3) % ITTM3 filter with a fixed number of iteration

% matlab code
yittm_fix_1_m = ITTM_fix(xw,2,1) % ITTM1 filter with a fixed number of iteration
yittm_fix_2_m = ITTM_fix(xw,2,2) % ITTM2 filter with a fixed number of iteration
yittm_fix_3_m = ITTM_fix(xw,2,3) % ITTM3 filter with a fixed number of iteration



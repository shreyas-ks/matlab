tic;clear all; close all; clc;
mean_x = 1; % Mean of X
mean_y = 2; % Mean of Y
var_x = 4; % Variance of X
var_y = 9; % Variance of Y
random1 = rand(1000,1);
random2 = rand(1000,1);
for i=1:1000
 % Geberate X and Y that are N(0,1) random variables and independent
 X(i) = sqrt( - 2*log(random1(i))).*cos(2*pi*random2(i) );
 Y(i) = sqrt( - 2*log(random1(i))).*sin(2*pi*random2(i) );
 % Scale them to a particular mean and variance 
 x(i) = sqrt(var_x)*X(i) + mean_x; % x~ N(M1,V1)
 y(i) = sqrt(var_y)*Y(i) + mean_y; % y~ N(M2,V2)
 A(i) = x(i) + y(i);
end
cov = cov(X, Y);
disp('cov(X,Y):');
disp(cov(2));
figure(1);
histogram(A);
yyaxis right;
title('Histogram of A');
xlabel('Samples of A');
ylabel('Frequency');
theo_m = 3;
exp_mean = 13;
dist = makedist('Normal', theo_m, exp_mean);
theo_pdf = pdf(dist, 0:20);
plot(theo_pdf);
yyaxis left;
disp('Box-Muller method');
disp('Theoretical Mean=3');
disp('Theoretical Variance=13');
disp('Exponential mean:');
disp(mean(A));
disp('Exponential variance:');
disp(var(A));
toc; % Read elapsed time from stopwatch

tic;
for i=0:999
  r1 = 2*rand() - 1;
  r2 = 2*rand() - 1;
  sum = r1^2 + r2^2;
  if (sum < 1)
    i = i+1;
    X(i+1) = sqrt(-2*log(sum)/sum) * r1;
    X(i+1) = sqrt(-2*log(sum)/sum) * r2;
  end
  i = i - 1;
end
for i=1:1000
  x(i) = sqrt(var_x)*X(i) + mean_x;
  y(i) = sqrt(var_y)*Y(i) + mean_y;
  A(i) = x(i) + y(i);
end
disp('Polar Marsaglia method');
disp('Theoretical Mean=3');
disp('Theoretical Variance=13');
disp('Exponential mean:');
disp(mean(A));
disp('Exponential variance:');
disp(var(A));
toc;

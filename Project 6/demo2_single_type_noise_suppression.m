%demo code 2: single type of noise suppession

%%
% created by Miao Zhenwei.
% Nanyang Technological Univerisity, ROSE center
% Webpage: https://sites.google.com/site/miaozhenwei/


% Please report bugs and/or send comments to Miao Zhenwei.
% zwmiao@ntu.edu.sg

%  Reference: Z. W. Miao and X. D. Jiang, "Additive and Exclusive Noise Suppression byIterative Trimmed and Truncated Mean Algorithm,� Signal Processing, vol. 99, pp. 147-158, June, 2014.

% Related papers

%             Z. W. Miao and X. D. Jiang, �Weighted Iterative Truncated Mean Filter,� IEEE Transactions on Signal Processing, Vol. 61, no. 16, pp. 4149-4160, August, 2013.
%
%             Z. W. Miao and X. D. Jiang, �Further Properties and a Fast Realization
%             of the Iterative Truncated Arithmetic Mean Filter� IEEE Transactions on Circuits and Systems-II,
%             vol. 59, no. 11, pp. 810-814, November 2012.
%
%             X.D. Jiang, "Iterative Truncated Arithmetic Mean Filter And Its Properties," IEEE Transactions
%             on Image Processing, vol. 21, no. 4, pp. 1537-1547, April 2012.

clc;clear;close all
%% compile the c code:
ITTM_compile


%% Noise signal
noisetype = 1;% 1: Laplacian noise, 2: Gaussian nosie:

max_ite   = 10;
n         = 49; %% filter size
Lo        = 100000; %% number of independent input data sets
L         = Lo*n;
if noisetype == 1
    %Laplacian noise with standard deviation equal 1
    xo = log(rand(L,1)./rand(L,1)); xo = xo./std(xo);
    CRLB          = 1/(2*n);
else
    %Gaussian noise with standard deviation equal 1
    xo = normrnd(0,1,L,1);  xo = xo./std(xo);
    CRLB          = 1/n;
end

%% 1. fixed number of iterations
for i=1:Lo
    xw = xo(i*n-n+1:i*n);
    ymedian(i)  = median(xw);%median
    ymean(i)    = mean(xw);  %mean   
    for ii_ite = 0:max_ite       %different number of iterations2
        yITTM_fix(ii_ite+1,i)  = ITTM_fix_c(xw,ii_ite,1);
    end    
end

vmean_norm      = ymean*ymean'/Lo/CRLB;
vmedian_norm    = ymedian*ymedian'/Lo/CRLB;
vittmfix_norm   = sum(yITTM_fix.^2,2)/Lo/CRLB;


xx = [0:max_ite]
subplot(1,2,1)
plot(xx,ones(max_ite+1,1),'black-','LineWidth',1.5)
hold on
plot(xx,ones(max_ite+1,1)*vmean_norm,'blue-diamond')
plot(xx,ones(max_ite+1,1)*vmedian_norm,'g-*')
plot(xx,vittmfix_norm,'r-o')
ylim([0.95 2.05])
legend('CRLB','mean','median','ITTM1')
ylabel('MSE normalized by CRLB')
xlabel('number of iterations $k$')
lh=findall(gca,'type','line');
set(lh,'LineWidth',2,'MarkerSize',8)
lh=findall(gca,'type','text');
set(lh,'FontSize',12)
lh=findall(gca,'type','Axes');
set(lh,'FontSize',12)




%% 2. ITTM filter terminated by the proposed stopping criterion

ww        = [9 25 49 81]';% four different window size
lw        = length(ww);
L         = Lo*max(ww);
if noisetype == 1
    %Laplacian noise with standard deviation equal 1
    xo = log(rand(L,1)./rand(L,1)); xo = xo./std(xo);
else
    %Gaussian noise with standard deviation equal 1
    xo = normrnd(0,1,L,1);  xo = xo./std(xo);
end

for ii_win = 1:lw
    n    = ww(ii_win); %% filter size
    ymedian = zeros(1,Lo);ymean = zeros(1,Lo);yITTM = zeros(1,Lo);
    for i=1:Lo
        xw = xo(i*n-n+1:i*n);
        ymedian(i)  = median(xw);
        ymean(i)    = mean(xw);
        yITTM(i)     = ITTM_c(xw,1);
    end
    
    if noisetype == 1
        CRLB          = 1/(2*n);%CRLB for Laplacian noise
    else
        CRLB          = 1/n;%CRLB for Gaussian noise
    end
    
    vmean_result(ii_win)    = ymean*ymean'/Lo/CRLB;
    vmedian_result(ii_win)  = ymedian*ymedian'/Lo/CRLB;
    vittm_result(ii_win)    = yITTM*yITTM'/Lo/CRLB;
end

subplot(1,2,2)
plot(ww,ones(lw,1),'black-')
hold on
plot(ww,vmean_result,'blue-diamond')
plot(ww,vmedian_result,'g-*')
plot(ww,vittm_result,'r-o')
% xlim([0.9 4.1])
ylim([0.95 2.05])

legend('CRLB','mean','median','ITTM1')
xlabel('filter size $n$')
lh=findall(gca,'type','line');
set(lh,'LineWidth',2,'MarkerSize',8)
lh=findall(gca,'type','text');
set(lh,'FontSize',12)
lh=findall(gca,'type','Axes');
set(lh,'FontSize',12)

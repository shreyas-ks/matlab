%% Histogram of Vector
% Generate 10,000 random numbers and create a histogram. The |histogram|
% function automatically chooses an appropriate number of bins to cover the
% range of values in |x| and show the shape of the underlying distribution.

% Copyright 2015 The MathWorks, Inc.

x = randn(10000,1);
h = histogram(x)

%%
% When you specify an output argument to the |histogram| function, it
% returns a histogram object. You can use this object to inspect the
% properties of the histogram, such as the number of bins or the width of
% the bins.

%%
% Find the number of histogram bins. 
nbins = h.NumBins